# Juice shop application on a Kubernetes cluster

The current project is a fork of the [Juice Shop](https://github.com/juice-shop/juice-shop), a [Node.js](https://nodejs.org/en) web application. The original intention was to utilize the application for conducting security tests in adherence to [OWASP](https://owasp.org) guidelines. However, in the current context, the objective is to deploy the application on a [Kubernetes](https://kubernetes.io) cluster within an ephemeral environment, while also conducting performance tests using [Locust](https://docs.locust.io/en/stable/what-is-locust.html). While the current project is largely based on the original one, it includes some minor modifications to align with the specific objectives of the current context. The original README can still be accessed [here](README.fork.md).

## System requirements

To get started, please follow these steps to install the latest versions of Node.js and Docker for your specific operating system:

Install the latest version of [Node.js](https://nodejs.org/en) for your OS. You can find the official Node.js website and download the installer suitable for your operating system. Follow the installation instructions provided by the Node.js installer to complete the setup.

Install the latest version of [Docker](https://www.docker.com/community-edition) for your OS. Visit the official Docker website and choose the Docker version compatible with your operating system. Download and run the Docker installer, following the installation instructions specific to your OS.

By installing the latest versions of Node.js and Docker, you'll have the necessary tools to proceed with your project.

## Run Juice Shop application without docker

```bash
cd PATH_TO_JUICE_SHOP_REPOSITORY
npm install # only has to be done before first start or when you change the source code
npm run start

> juice-shop@14.5.1 start
> node build/app

info: Server listening on port 3000
```

Browse to <http://localhost:3000>

## Run Juice Shop application with docker

To build and push the Docker image of the application to the GitLab registry, please follow these instructions:

Start by logging in

```bash
docker login --username $GITLAB_USER --password $GITLAB_PERSONAL_ACCESS_TOKEN registry.gitlab.com
```

Then build and push

```bash
docker build \
    --no-cache \
    --progress=plain \
    --tag "registry.gitlab.com/aurelienlair/juice-shop:latest" \
    .

docker push registry.gitlab.com/aurelienlair/juice-shop:latest
```

You can double check locally by running the docker image in order to check if it's working properly

```bash
docker run --publish 3000:3000 registry.gitlab.com/aurelienlair/juice-shop:latest

info: All dependencies in ./package.json are satisfied (OK)
info: Chatbot training data botDefaultTrainingData.json validated (OK)
info: Detected Node.js version v18.15.0 (OK)
info: Detected OS linux (OK)
info: Detected CPU x64 (OK)
info: Configuration default validated (OK)
info: Entity models 19 of 19 are initialized (OK)
info: Required file server.js is present (OK)
info: Required file index.html is present (OK)
info: Required file styles.css is present (OK)
info: Required file main.js is present (OK)
info: Required file tutorial.js is present (OK)
info: Required file polyfills.js is present (OK)
info: Required file runtime.js is present (OK)
info: Required file vendor.js is present (OK)
info: Port 3000 is available (OK)
info: Server listening on port 3000
```

Browse to <http://localhost:3000>

## Run Juice Shop application on a kubernetes cluster

Have a look to [this](https://gitlab.com/aurelienlair/kubernetes-orchestrator) repository to know how to run juice shop on a k8s cluster

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of a run command
```

| Type | Description |
|------|-------------|
| `style` | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build` | Changes to the build process |
| `chore` | Changes to the build process or auxiliary tools and libraries such as documentation generation |
| `docs` | Documentation updates |
| `feat` | New features |
| `fix`  | Bug fixes |
| `refactor` | Code refactoring |
| `test` | Adding missing tests |
| `perf` | A code change that improves performance |

